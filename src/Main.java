import java.time.LocalDate;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<LocalDate> date = new ArrayList<>();
        date.add(LocalDate.of(2004,7,1));
        date.add(LocalDate.of(2007,8,1));
        date.add(LocalDate.of(2005,8,2));
        date.add(LocalDate.of(2012,1,2));
        date.add(LocalDate.of(2008,5,2));
        date.add(LocalDate.of(2034,3,2));
        date.add(LocalDate.of(2034,10,1));
        date.add(LocalDate.of(2034,1,15));
        date.add(LocalDate.of(2032,5,3));
        DateSorter dateSorter = new DateSorter();
        System.out.println(dateSorter.sortDates(date));
    }
}